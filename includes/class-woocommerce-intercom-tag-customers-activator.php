<?php

/**
 * Fired during plugin activation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Intercom_Tag_Customers
 * @subpackage Woocommerce_Intercom_Tag_Customers/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Woocommerce_Intercom_Tag_Customers
 * @subpackage Woocommerce_Intercom_Tag_Customers/includes
 * @author     # <#>
 */
class Woocommerce_Intercom_Tag_Customers_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
