<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit7e2a52705fe9ea5ed94f4f2778d426e3
{
    public static $files = array (
        '5640faacc2b60a961e5376b0effdd7ef' => __DIR__ . '/..' . '/formr/formr/class.formr.php',
    );

    public static $prefixLengthsPsr4 = array (
        'R' => 
        array (
            'ReCaptcha\\' => 10,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'ReCaptcha\\' => 
        array (
            0 => __DIR__ . '/..' . '/google/recaptcha/src/ReCaptcha',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit7e2a52705fe9ea5ed94f4f2778d426e3::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit7e2a52705fe9ea5ed94f4f2778d426e3::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit7e2a52705fe9ea5ed94f4f2778d426e3::$classMap;

        }, null, ClassLoader::class);
    }
}
