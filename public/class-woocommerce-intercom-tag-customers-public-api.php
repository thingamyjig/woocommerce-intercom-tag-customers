<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Intercom_Tag_Customers
 * @subpackage Woocommerce_Intercom_Tag_Customers/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_Intercom_Tag_Customers
 * @subpackage Woocommerce_Intercom_Tag_Customers/public
 * @author     # <#>
 */


class Woocommerce_Intercom_Tag_Customers_Public_Api {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * The api key.
     *
     */
    public $api_key;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->api_key = get_option('wc_ic_form_api_key');
	}

    /**
     * Get API key
     */
    public function get_api_key() {
        return $this->api_key;
    }

    /**
     * Send API request
     */
    public function wp_ic_remote_post ($type, $body, $contact_id = NULL) {

        $remote_url = 'https://api.intercom.io/contacts/';
        $method = 'POST';

	    if ($type == 'update') {
	        $method = 'PUT';
            $remote_url = 'https://api.intercom.io/contacts/' . $contact_id;
        }

        $headers_params = array(
            'Authorization' => 'Bearer ' . $this->get_api_key(),
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        );

        return wp_remote_request(
            $remote_url,
            array(
                'headers' => $headers_params,
                'body' => $body,
                'method' => $method
            )
        );
    }

}
