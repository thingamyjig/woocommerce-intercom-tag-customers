<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           Woocommerce_Intercom_Tag_Customers
 *
 * @wordpress-plugin
 * Plugin Name:       WooCommerce - Intercom Tag Customers
 * Plugin URI:        #
 * Description:       This plugin creates a submission form that interacts with Intercom's API.
 * Version:           1.0.1
 * Author:            #
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       woocommerce-intercom-tag-customers
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WOOCOMMERCE_INTERCOM_TAG_CUSTOMERS_VERSION', '1.0.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-woocommerce-intercom-tag-customers-activator.php
 */
function activate_woocommerce_intercom_tag_customers() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-intercom-tag-customers-activator.php';
	Woocommerce_Intercom_Tag_Customers_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-woocommerce-intercom-tag-customers-deactivator.php
 */
function deactivate_woocommerce_intercom_tag_customers() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-intercom-tag-customers-deactivator.php';
	Woocommerce_Intercom_Tag_Customers_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_woocommerce_intercom_tag_customers' );
register_deactivation_hook( __FILE__, 'deactivate_woocommerce_intercom_tag_customers' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-woocommerce-intercom-tag-customers.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_woocommerce_intercom_tag_customers() {

	$plugin = new Woocommerce_Intercom_Tag_Customers();
	$plugin->run();

}
run_woocommerce_intercom_tag_customers();
