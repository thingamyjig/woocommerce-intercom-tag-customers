<?php
/**
 * Extends the WC_Settings_Page class
 *
 * @link        #
 * @since       1.0.0
 *
 * @package     Atum_Multi_Inventory_Extension_Admin_WC_Settings
 * @subpackage  Atum_Multi_Inventory_Extension_Admin_WC_Settings/admin
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'Woocommerce_Intercom_Tag_Customers_Admin_Settings' ) ) {

    /**
     * Settings class
     *
     * @since 1.0.0
     */
    class Woocommerce_Intercom_Tag_Customers_Admin_Settings extends WC_Settings_Page {

        /**
         * Constructor
         * @since  1.0
         */
        public function __construct() {

            $this->id    = 'wc-ic-form';
            $this->label = __( 'Intercom Newsletter Signup', $this->id );
            // Define all hooks instead of inheriting from parent
            add_filter( 'woocommerce_settings_tabs_array', array( $this, 'add_settings_page' ), 20 );
            add_action( 'woocommerce_settings_' . $this->id, array( $this, 'output' ) );
            add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'save' ) );
            add_action( 'woocommerce_sections_' . $this->id, array( $this, 'output_sections' ) );
        }


        /**
         * Get sections.
         *
         * @return array
         */
        public function get_sections() {

            $sections = array(
                '' => __( 'Settings', $this->id ),
            );

            return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
        }


        /**
         * Get settings array
         *
         * @return array
         */
        public function get_settings($current_section = '') {
            global $current_section;
            $settings = array();
            if ($current_section === '') {
                $prefix = 'wc_ic_form_';
                include 'partials/woocommerce-intercom-tag-customers-admin-display-settings.php';
            }
            return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );
        }

        /**
         * Output the settings
         */
        public function output() {
            global $current_section;
            $settings = $this->get_settings( $current_section );
            WC_Admin_Settings::output_fields( $settings );

        }



        /**
         * Validate the enabled key
         */
        public function validate_general_fields( $fields ) {
            return true;
        }

        /**
         * Save settings
         *
         * @since 1.0
         */
        public function save() {
            global $current_section;
            $settings = $this->get_settings( $current_section );
            if ($current_section == '') {
                $validate = $this->validate_general_fields($_POST); // custom validation for general tab
                if ($validate)
                    WC_Admin_Settings::save_fields( $settings );
            }

        }

    }

}

return new Woocommerce_Intercom_Tag_Customers_Admin_Settings();