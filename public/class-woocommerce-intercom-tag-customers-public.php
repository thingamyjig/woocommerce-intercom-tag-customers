<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Woocommerce_Intercom_Tag_Customers
 * @subpackage Woocommerce_Intercom_Tag_Customers/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Woocommerce_Intercom_Tag_Customers
 * @subpackage Woocommerce_Intercom_Tag_Customers/public
 * @author     # <#>
 */

use \ReCaptcha\ReCaptcha as ReCaptcha;

class Woocommerce_Intercom_Tag_Customers_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * Google Recaptcha
     *
     * @since    1.0.0
     * @access   public
     */
    public $site_key;
    public $secret_key;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->site_key = get_option('wc_ic_form_site_key');
        $this->secret_key = get_option('wc_ic_form_secret_key');
	}

    /**
     * Google Recaptcha get_site_key
     *
     * @since    1.0.0
     */
    public function get_site_key() {
        return $this->site_key;
    }

    /**
     * Google Recaptcha get_secret_key
     *
     * @since    1.0.0
     */
    public function get_secret_key() {
        return $this->secret_key;
    }

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/woocommerce-intercom-tag-customers-public.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
        wp_register_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/woocommerce-intercom-tag-customers-public.js', array( 'jquery' ), $this->version, TRUE );
        wp_localize_script( $this->plugin_name, 'wc_ic_ajax_object',
            array(
                'ajax_url'   => admin_url( 'admin-ajax.php' ),
                'ajax_nonce' => wp_create_nonce( 'wc_ic_ajax_nonce' )
            )
        );
        wp_enqueue_script( $this->plugin_name );
	}

    /**
     * Registers the shortcodes
     * @since 1.0.0
     */
    public function register_shortcodes() {
        add_shortcode('wc_ic_subscribe_form', array($this, 'shortcode_wc_ic_subscribe_form'));
    }

    /**
     * Get logged-in user email
     * @since 1.0.0
     */
    public function populate_email_input() {
        $current_user = wp_get_current_user();
        $value = $current_user ? $current_user->user_email : '';
        return $value;
    }

    /**
     * Shortcode: Show the form
     *
     * @since    1.0.0
     */
    public function shortcode_wc_ic_subscribe_form() {
        $form = new Formr\Formr('bulma');
        $form->name = 'wc_ic_subscribe';
        $form->action = '';
        $form->hidden('action','wc_ic_subscribe');
        $form->required = 'email,province';
        $default = $this->populate_email_input();
        ob_start();
            // Start HTML ouput
        echo '<div class="wc_ic_subscribe_form_wrapper">';
        echo '<div class="wc_ic_subscribe woocommerce-message" role="alert">' . __('Your subscription preferences have been updated.', $this->plugin_name) . '</div>';
        echo '<div class="wc_ic_subscribe woocommerce-error" role="alert"><ul><li class="error">' . __('Something went wrong. Please try again.', $this->plugin_name) . '</li></ul></div>';
            $form->form_open();
            $form->email('email','Your Email',$default);
            $form->label_open('province');
            $form->label_close('Province or Territory');
            $form->select('province','Province','','','','','','provinces');
            echo '<div class="g-recaptcha form-field" data-sitekey="' . $this->get_site_key() . '"></div>';
            $form->submit_button();
            $form->form_close();
            echo '</div>';
            echo '<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>';
        return ob_get_clean();
    }

    /**
     * Process form submission
     *
     * @since    1.0.0
     */
    public function wc_ic_subscribe() {

        check_ajax_referer( 'wc_ic_ajax_nonce', 'nonce' );
        $data = $_POST;
        $email = sanitize_email( $data['email'] );
        $province = sanitize_text_field( $data['province'] );
        $recaptcha_response = $data['recaptcha'];

        // Check Recapcha
        if (empty($recaptcha_response)) {
            wp_send_json_error( 'Recaptcha cannot be blank. Try again.');
        } else {
            $recaptcha_secret = $this->get_secret_key();
            $recaptcha_obj = new ReCaptcha($recaptcha_secret);
            $resp = $recaptcha_obj->setExpectedHostname($_SERVER['SERVER_NAME'])->verify($recaptcha_response, $_SERVER['REMOTE_ADDR']);
            if (!$resp->isSuccess()) {
                wp_send_json_error( 'Something went wrong with your Recaptcha input. Try again. (Code '.$resp->getErrorCodes());
            }
        }

        // Is valid email?
        if (!is_email($email)) {
            wp_send_json_error( 'This is not a valid email!' );
        }
        else {
            // Create body data
            $body = wp_json_encode( array(
                "role" => "user",
                "email" => $email,
                "custom_attributes" => array(
                    "newsletter_signup" => true,
                    "newsletter_province" => $province,
                )
            ) );

            $api_request = new Woocommerce_Intercom_Tag_Customers_Public_Api($plugin_name, $version);
            $request =  $api_request->wp_ic_remote_post('new', $body);

            if ( ! is_wp_error( $request ) ) {
                $request = json_decode( wp_remote_retrieve_body( $request ), true );
                // Already exists
                if ($request['type'] == 'error.list') {
                    $message = $request['errors'][0]['message'];
                    // Extract contact_id from error
                    $contact_id = substr($message, strpos($message, "id=") + 3);
                    // Update contact
                    $request =  $api_request->wp_ic_remote_post('update', $body, $contact_id);
                    if ( ! is_wp_error( $request ) ) {
                        wp_send_json_success($contact_id);
                    } else {
                        wp_send_json_error( __('Something went wrong. Please try again.', $this->plugin_name) );
                    }
                }
                // New Contact
                else {
                    wp_send_json_success($request['id']);
                }

            } else {
                wp_send_json_error( __('Something went wrong. Please try again.', $this->plugin_name) );
            }
        }
        die();
    }
}
