<?php

/*
https://www.skyverge.com/blog/add-custom-options-to-woocommerce-settings/
*/

$settings = array(
    array(
        'name' => __( 'General Configuration', $this->id ),
        'type' => 'title',
        'id'   => $prefix . 'general_config_settings'
    ),
    array(
        'id'        => $prefix . 'api_key',
        'name'      => __( 'Intercom App API Key', $this->id ),
        'type'      => 'text',
        'desc_tip'  => __( 'The API key for your Intercom App', $this->id)
    ),
    array(
        'id'        => $prefix . 'site_key',
        'name'      => __( 'Recaptcha Site Key', $this->id ),
        'type'      => 'text',
        'desc_tip'  => __( '', $this->id)
    ),
    array(
        'id'        => $prefix . 'secret_key',
        'name'      => __( 'Recaptcha Secret Key', $this->id ),
        'type'      => 'text',
        'desc_tip'  => __( '', $this->id)
    ),
    array(
        'id'        => '',
        'name'      => __( 'General Configuration', $this->id ),
        'type'      => 'sectionend',
        'desc'      => '',
        'id'        => $prefix . 'general_config_settings'
    ),
);
?>