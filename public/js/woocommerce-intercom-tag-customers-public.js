(function( $ ) {
	'use strict';

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(function() {
		if ($('div.wc_ic_subscribe_form_wrapper').length) {
			var success_msg = $('.wc_ic_subscribe.woocommerce-message');
			var error_msg = $('.wc_ic_subscribe.woocommerce-error');
			success_msg.hide();
			error_msg.hide();
			$('form[name=wc_ic_subscribe]').submit(function (e) {
				e.preventDefault();
				// Responsive button
				var curSubmit = $('form[name=wc_ic_subscribe] button#submit');
				curSubmit.width(curSubmit.width()).text('...');
				curSubmit.prop('disabled', true);
				// Gather data
				var data = {
					action: 'wc_ic_subscribe',
					email: $('#email').val(),
					province: $('#province').val(),
					recaptcha: $('#g-recaptcha-response').val(),
					nonce: wc_ic_ajax_object.ajax_nonce
				};
				// Send Ajax
				$.post(wc_ic_ajax_object.ajax_url, data, function (response) {
					if (response.success === true) {
						//console.log(response);
						$("form[name=wc_ic_subscribe]").trigger('reset');
						curSubmit.width(curSubmit.width()).text('Submit');
						curSubmit.prop('disabled', false);
						error_msg.hide();
						success_msg.slideDown();
					} else if (response.success === false) {
						//console.log(response);
						curSubmit.width(curSubmit.width()).text('Submit');
						curSubmit.prop('disabled', false);
						$('.wc_ic_subscribe.woocommerce-error ul li').text(response.data);
						error_msg.slideDown();
						success_msg.hide();
					}
				});
			});
		}
	});
})( jQuery );